import { Component, OnInit } from '@angular/core';
import {GoogleChartComponent} from './google-chart/google-chart.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  // https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2

  public categories = ['Overall','Human Rights','GDP','Diseases','Death Penalty'];

  public map_ChartData_Overall_2017 = [
        ['Country', 'Score'],
        ['Germany', 600],
        ['United States', 300],
        ['Brazil', 400],
        ['Canada', 700],
        ['France', 500],
        ['RU', 200],
        ['CH',700]
  ];
  public map_ChartData_Overall_2016 = [
        ['Country', 'Score'],
        ['Germany', 600],
        ['United States', 300],
        ['Brazil', 400],
        ['Canada', 700],
        ['France', 500],
        ['RU', 200],
        ['CH',700]
  ];
  public map_ChartData_HumanRights = [
      ['Country', 'Score'],
      ['Germany', 700],
      ['United States', 400],
      ['Brazil', 400],
      ['Canada', 700],
      ['France', 500],
      ['RU', 200],
      ['CH',700]
  ];
  public map_ChartData_GDP = [
      ['Country', 'Score'],
      ['Germany', 700],
      ['United States', 400],
      ['Brazil', 400],
      ['Canada', 700],
      ['France', 500],
      ['RU', 200],
      ['CH',700]
  ];
  public map_ChartData_Diseases = [
      ['Country', 'Score'],
      ['Germany', 700],
      ['United States', 400],
      ['Brazil', 400],
      ['Canada', 700],
      ['France', 500],
      ['RU', 200],
      ['CH',700]
  ];
  public map_ChartData_DeathPenalty = [
      ['Country', '',{role: 'tooltip', p:{html:true}}],
      ['Germany', null,''],
      ['United States', 3,'Executions: 1452'], // https://deathpenaltyinfo.org/executions-year
      ['Brazil', 2,''],
      ['Canada', null,''],
      ['France', null,''],
      ['AU', null,''],
      ['MG', null,''],
      ['NZ', null,''],
      ['RU', 1,''],
      ['CN',3,''],
      ['EG',3,''],
      ['IN',3,''],
      ['JP',3,''],
      ['CH',null,'']
  ];
  public map_ChartOptions = {
    height: 500,
    width: 800,
    region: 'world',
    sizeAxis: { minValue: 0, maxValue: 1000 },
    colorAxis: {colors: ['red', 'green']},
    datalessRegionColor: '#f8bbd0',
    defaultColor: '#f5f5f5',
  };
  public map_ChartOptions_DeathPenalty = {
    height: 500,
    width: 800,
    region: 'world',
    legend: 'none',
    tooltip: {textStyle: {color: '#000000'}, showColorCode: false},
    colorAxis: {colors: ['yellow', 'orange', 'red']},
    datalessRegionColor: '#f8bbd0',
    defaultColor: '#f5f5f5',
  };

  constructor() {
  }

  ngOnInit() {

  }

}