// Get a reference to the database service
var database = firebase.database();

var query = firebase.database().ref("categories").orderByKey();
query.once("value")
  .then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      // key will be "ada" the first time and "alan" the second time
      var key = childSnapshot.key;
      // childData will be the actual contents of the child
      var childData = childSnapshot.val();

      console.log(key);
      console.log(childData);
  });
});

function drawOverallChart(index){
    var data = [];
    var d1 = google.visualization.arrayToDataTable([
        ['Country', 'Score'],
        ['Germany', 600],
        ['United States', 300],
        ['Brazil', 400],
        ['Canada', 700],
        ['France', 500],
        ['RU', 200],
        ['CH',700]
    ]);
    var d2 = google.visualization.arrayToDataTable([
        ['Country', 'Score'],
        ['Germany', 200],
        ['United States', 200],
        ['Brazil', 200],
        ['Canada', 200],
        ['France', 200],
        ['RU', 200],
        ['CH',200]
    ]);
    var d3 = google.visualization.arrayToDataTable([
        ['Country', 'Score'],
        ['Germany', 700],
        ['United States', 700],
        ['Brazil', 700],
        ['Canada', 700],
        ['France', 700],
        ['RU', 700],
        ['CH',700]
    ]);
    data.push(d1);
    data.push(d2);
    data.push(d3);

    var options = {};

    var chart = new google.visualization.GeoChart(document.getElementById('map_chart_overall'));

    chart.draw(data[index], options);
}

function drawGDPChart(){
    
}

function drawDeathPenaltyChart(){
    
}

$(document).ready(function () {

    $("#ex01").slider();
    $("#ex01").on("change", function(event) {
        var a = event.value.newValue;
        var b = event.value.oldValue;

        var changed = !($.inArray(a[0], b) !== -1 && 
                        $.inArray(a[1], b) !== -1 && 
                        $.inArray(b[0], a) !== -1 && 
                        $.inArray(b[1], a) !== -1 && 
                        a.length === b.length);

        if(changed) {
            var index = parseInt(a) - 1;
            drawOverallChart(index);
        }
    });

});