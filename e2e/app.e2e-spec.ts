import { WorldIndexPage } from './app.po';

describe('world-index App', () => {
  let page: WorldIndexPage;

  beforeEach(() => {
    page = new WorldIndexPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
